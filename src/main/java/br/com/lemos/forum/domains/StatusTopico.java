package br.com.lemos.forum.domains;

public enum StatusTopico {
	
	NAO_RESPONDIDO,
	NAO_SOLUCIONADO,
	SOLUCIONADO,
	FECHADO;

}
