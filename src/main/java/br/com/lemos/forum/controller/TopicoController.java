package br.com.lemos.forum.controller;



import java.net.URI;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.lemos.forum.controller.dto.TopicoDTO;
import br.com.lemos.forum.controller.dto.TopicoDetalheDTO;
import br.com.lemos.forum.controller.form.TopicoAtualizacaoForm;
import br.com.lemos.forum.controller.form.TopicoForm;
import br.com.lemos.forum.domains.Topico;
import br.com.lemos.forum.repositories.CursoRepository;
import br.com.lemos.forum.repositories.TopicoRepository;

@RestController
@RequestMapping("/topicos")
public class TopicoController {
	
	@Autowired
	TopicoRepository topicoRepository;
	
	@Autowired
	CursoRepository cursoRepository;
	
//	@GetMapping
//	public Page<TopicoDTO> listar(@RequestParam(required = false) String nomeCurso,@RequestParam  int pagina, @RequestParam int qtd, @RequestParam String ordenacao) {
//		
//		Pageable paginacao = PageRequest.of(pagina, qtd, Direction.ASC, ordenacao);
//		if (nomeCurso == null) {
//			return TopicoDTO.converter(topicoRepository.findAll(paginacao));
//		} else {
//			return TopicoDTO.converter(topicoRepository.findByCursoNome(nomeCurso, paginacao));
//		}
//	}
	
	@GetMapping
	@Cacheable (value = "listaDeTopicos") //Habilita o cache para esse metodo
	public Page<TopicoDTO> listar(@RequestParam(required = false) String nomeCurso, @PageableDefault(sort = "id", direction = Direction.ASC, page = 0, size = 10) Pageable paginacao) {
		
		if (nomeCurso == null) {
			return TopicoDTO.converter(topicoRepository.findAll(paginacao));
		} else {
			return TopicoDTO.converter(topicoRepository.findByCursoNome(nomeCurso, paginacao));
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TopicoDetalheDTO> detalhar(@PathVariable Long id) {
		
		Optional<Topico> optional = topicoRepository.findById(id);
		
		if(optional.isPresent()) {
			return ResponseEntity.ok(new TopicoDetalheDTO(optional.get()));
		}
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@Transactional
	@CacheEvict(value = "listaDeTopicos", allEntries = true) //Invalida o cache no caso de novo cadastro
	public ResponseEntity<TopicoDTO> cadastrar(@RequestBody @Valid TopicoForm form, UriComponentsBuilder uriBuilder) {
		
		Topico topico = form.converter(cursoRepository);
		topicoRepository.save(topico);
		URI uri = uriBuilder.path("/topicos/{id}").buildAndExpand(topico.getId()).toUri();
		return ResponseEntity.created(uri).body(new TopicoDTO(topico));
	}
	
	@PutMapping("/{id}")
	@Transactional //Serve para avisar o metodo que é necessario fazer uma transicao no fim da execucao
	@CacheEvict(value = "listaDeTopicos", allEntries = true) //Invalida o cache no caso de novo cadastro
	public ResponseEntity<TopicoDTO> atualizar(@PathVariable Long id, @RequestBody @Valid TopicoAtualizacaoForm form, UriComponentsBuilder uriBuilder) {
		
		Optional<Topico> optional = topicoRepository.findById(id);
		
		if(optional.isPresent()) {
			Topico topico = form.atualizar(id, topicoRepository);
			return ResponseEntity.ok(new TopicoDTO(topico));
		}
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	@Transactional
	@CacheEvict(value = "listaDeTopicos", allEntries = true) //Invalida o cache no caso de novo cadastro
	public ResponseEntity<?> remover(@PathVariable Long id){
		
		Optional<Topico> optional = topicoRepository.findById(id);
		if(optional.isPresent()) {
			topicoRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
}
