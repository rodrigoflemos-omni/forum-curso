package br.com.lemos.forum.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.lemos.forum.domains.Curso;

public interface CursoRepository extends JpaRepository<Curso, Long> {

	Curso findByNome(String nomeCurso);

}
