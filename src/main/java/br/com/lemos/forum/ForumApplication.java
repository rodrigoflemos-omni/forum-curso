package br.com.lemos.forum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication
@EnableSpringDataWebSupport /* Habilita a obtencao dos parametros de paginacao e ordenacao da URL*/
@EnableCaching /* Habilita o uso de cache. Em producao, eh necessario procurar providers de cache. 
                  O provider padrao do Spring Boot nao funciona bem em producao
                  Utilizar cache apenas para carregar tabelas que nao sao atualizadas com frequencia. Armazenar cache e limpar em todos os momentos, pode derrubar a performance da aplicacao */
public class ForumApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForumApplication.class, args);
	}

}
