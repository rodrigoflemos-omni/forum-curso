INSERT INTO usuario (nome,email,senha) VALUES ('Rodrigo Lemos','rodrigoflemos@gmail.com','$2a$10$hO7F12H9DBOqp3EROGc1XezI/1JlBWXPzLQ426p8i3whmQsLo4Ka6')


INSERT INTO curso (nome,categoria) VALUES ('Java','Programação')
INSERT INTO curso (nome,categoria) VALUES ('DevOps','Administração de Sistemas')

INSERT INTO topico (titulo,mensagem, data_criacao,status, autor_id, curso_id) VALUES ('Como criar um Map','Estou com dúvida sobre como criar um map','2019-07-19 17:00:00','NAO_SOLUCIONADO',1,1)
INSERT INTO topico (titulo,mensagem, data_criacao,status, autor_id, curso_id) VALUES ('Como encerrar o Java','Como faço pra encerrar um programa Java?','2019-07-20 17:00:00','NAO_RESPONDIDO',1,1)
INSERT INTO topico (titulo,mensagem, data_criacao,status, autor_id, curso_id) VALUES ('O que é DevOps','Qual é a definição de DevOps?','2019-07-19 17:00:00','NAO_RESPONDIDO',1,2)
INSERT INTO topico (titulo,mensagem, data_criacao,status, autor_id, curso_id) VALUES ('DevOps Tools','Quais as ferramentas mais importantes de devops?','2019-07-20 17:00:00','NAO_RESPONDIDO',1,2)

INSERT INTO resposta (mensagem, topico_id, data_criacao, autor_id) VALUES ('Resposta1', 1, '2019-07-21 09:00:00', 1)
INSERT INTO resposta (mensagem, topico_id, data_criacao, autor_id) VALUES ('Resposta2', 1, '2019-07-21 09:00:00', 1)